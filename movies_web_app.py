import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode
from decimal import Decimal


application = Flask(__name__)
app = application


def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE movies(id INT UNSIGNED NOT NULL AUTO_INCREMENT, year INT UNSIGNED, title TEXT, director TEXT, actor TEXT, release_date TEXT, rating DOUBLE, PRIMARY KEY (id))'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        #try:
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
        #except Exception as exp1:
        #    print(exp1)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
        #populate_data()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)


def query_data():

    db, username, password, hostname = get_db_creds()

    print("Inside query_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    cur.execute("SELECT year, title, director, actor, release_date, rating FROM movies")
    entries = [dict(greeting=row[0]) for row in cur.fetchall()]
    return entries



@app.route('/add_movie', methods=['POST'])
def add_to_db():
    print("Received request.")
    # print(request.form['movies'])
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = request.form['rating']



    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    m = []
    # Check if title exists
    try:
        sqlcommand = "SELECT * FROM movies WHERE title=%s"
        values = (title, )
        cur.execute(sqlcommand, values)
        if(len(cur.fetchall()) > 0):
            msg = "Movie " + title + " already exists"
            m.append(msg)
            return render_template('index.html', message=m)
    except Exception as exp:
        msg = "Movie " + title + " could not be inserted - " + str(exp)
        m.append(msg)
        return render_template('index.html', message=m)
        
    # Add the movie 
    m = []
    try:
        sqlcommand = "INSERT INTO movies (year, title, director, actor, release_date, rating) VALUES (%s, %s, %s, %s, %s, %s)"
        values = (year, title, director, actor, release_date, rating)
        cur.execute(sqlcommand, values)
        msg = "Movie " + title + " successfully inserted"
        m.append(msg)
    except Exception as exp:
        msg = "Movie " + title + " could not be inserted - " + str(exp)
        m.append(msg)


    cnx.commit()
    return render_template('index.html', message=m)


@app.route('/update_movie', methods=['POST'])
def put_to_db():
    print("Received request.")
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = request.form['rating']


    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    m = []
    try:
        sqlcommand = "UPDATE movies SET year = %s, title = %s, director = %s, actor = %s, release_date = %s, rating = %s WHERE title = %s"
        values = (year, title, director, actor, release_date, rating, title)
        cur.execute(sqlcommand, values)
        numRows = cur.rowcount
        msg = ""
        if(numRows > 0):
            msg = "Movie " + title + " successfully updated"
        else:
            msg = "Movie " + title + " does not exist; invalid title"
        m.append(msg)
    except Exception as exp:
        msg = "Movie " + title + " could not be updated - " + str(exp)
        m.append(msg)

    
    cnx.commit()
    return render_template('index.html', message=m)


@app.route('/search_movie', methods=['GET'])
def get_movie():
    print("Received request.")
    actor = request.args.get('search_actor')


    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)


    cur = cnx.cursor()
    m = []
    try:
        sqlcommand = "SELECT title, year, actor FROM movies WHERE actor=%s"
        value = (actor,)
        cur.execute(sqlcommand, value)
        data = cur.fetchall()
        m = []
        if(len(data) < 1):
            msg = "No movies found for actor: " + actor
            m.append(msg)
            return render_template('index.html', message=m)

        for row in data:
            title = row[0]
            year = row[1]
            actor = row[2]
            line = title + ", " + str(year) + ", " + actor
            m.append(line)

    except Exception as exp:
        msg = "Cannot find movie(s): " + str(exp)
        m.append(msg)


    cnx.commit()
    return render_template('index.html', message=m)


@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    print("Received request.")
    title = request.form['delete_title']


    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    m = []
    try:
        sqlcommand = "DELETE FROM movies WHERE title = %s"
        value = (title, )
        cur.execute(sqlcommand, value)
        numRows = cur.rowcount
        
        msg = ""
        if numRows > 0:
            msg = "Movie " + title + " successfully deleted"
        else:
            msg = "Movie " + title + " does not exist"

        m.append(msg)
    except Exception as exp:
        msg = "Movie "+ title +" could not be deleted - " + str(exp)
        m.append(msg)

    cnx.commit()
    return render_template('index.html', message=m)


@app.route('/highest_rating', methods=['GET'])
def get_high():
    print("Received request.")

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    sqlcommand = "SELECT max(rating) FROM movies"
    cur.execute(sqlcommand)
    result = cur.fetchall()
    max_rating = 0
    for r in result:
        max_rating = r[0]

    sqlcommand = "SELECT title, year, actor, director, rating FROM movies WHERE rating=%s"
    values = (max_rating, )
    cur.execute(sqlcommand, values)
    m = []
    for row in cur.fetchall():
        title = row[0]
        year = row[1]
        actor = row[2]
        director = row[3]
        rating = row[4]
        line = title + ", " + str(year) + ", " + actor + ", " + director + ", " + str(rating)
        m.append(line)

    cnx.commit()
    return render_template('index.html', message=m)




@app.route('/lowest_rating', methods=['GET'])
def get_low():
    print("Received request.")

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    sqlcommand = "SELECT min(rating) FROM movies"
    cur.execute(sqlcommand)
    result = cur.fetchall()
    min_rating = 0
    for r in result:
        min_rating = r[0]

    sqlcommand = "SELECT title, year, actor, director, rating FROM movies WHERE rating=%s"
    values = (min_rating, )
    cur.execute(sqlcommand, values)
    m = []
    for row in cur.fetchall():
        title = row[0]
        year = row[1]
        actor = row[2]
        director = row[3]
        rating = row[4]
        line = title + ", " + str(year) + ", " + actor + ", " + director + ", " + str(rating)
        m.append(line)

    cnx.commit()
    return render_template('index.html', message=m)




@app.route("/")
def hello():
    print("Inside hello")
    print("Printing available environment variables")
    print(os.environ)
    print("Before displaying index.html")
    try:
        print("---------" + time.strftime('%a %H:%M:%S'))
        print("Before create_table global")
        create_table()
        print("After create_data global")
    except Exception as exp:
        print("Got exception %s" % exp)
        conn = None
    return render_template('index.html', message="")


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
